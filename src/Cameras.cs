﻿using System;
using System.Collections.Generic;

namespace src
{

    public class CameraShower
    {
        private Camera camera;

        public CameraShower(Camera camera)
        {
            this.camera = camera;
        }

        public string Show()
        {          
           return string.Format("Showing: {0}", camera.GetName());
          
        }
    }

    public class CameraNC2 : Camera
    { 
        public CameraNC2(string name) : base(name)
        {
        }
    }
    public class CameraNC1 : Camera
    {
        public CameraNC1(string name) : base(name)
        {
        }

    }
    public class UnsupportedCamera : Camera
    {
        public UnsupportedCamera(string name) : base(name)
        {
        }

        public override string GetName()
        {
             return "not supported";
        }
    }

    public abstract class Camera
    {
        protected readonly string name;

        public Camera(string name)
        {
            this.name = name;
        }
        public virtual string GetName()
        {
            return name;
        }
    }
}
