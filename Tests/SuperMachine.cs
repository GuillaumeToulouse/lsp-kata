using System;

namespace Tests
{
    public class MachineACafeAutomatique
    {
        MachineACafé machineInterne;

      

        public MachineACafeAutomatique(Boolean enGrain)
        {
            if (enGrain)
                machineInterne = new MachineAGrains();
            else
                machineInterne = new MachineACaféMoulu();
        }


//faire un diagramme UML
//regler le pb de Liskov  (doit pouvoir substituer n'importe quel fils à la classe de base)
//se poser le pb de qui fait le choix du café
        internal bool FaisUnCafé()
        {
            if (this.machineInterne is MachineAGrains)
                ((MachineAGrains)machineInterne).ChargerReservoir(new CaféEnGrains());
            else
                ((MachineACaféMoulu)machineInterne).ChargerReservoir(new CaféMoulu());

            //(MachineACafé)machineInterne.ChargerReservoir(...)

            return machineInterne.EnvoyerEauChaude();
        }
    }

    public abstract class MachineACafé
    {
        public abstract bool EnvoyerEauChaude();
    }

    public class MachineAGrains : MachineACafé
    {
        public void ChargerReservoir(CaféEnGrains contenu)
        {

        }

        public override bool EnvoyerEauChaude()
        {
            return true;
        }
    }

    public class MachineACaféMoulu : MachineACafé
    {
        public override bool EnvoyerEauChaude()
        {
            return true;
        }
        public void ChargerReservoir(CaféMoulu contenu)
        {

        }
    }

    public class CaféMoulu
    {
    }

    public class CaféEnGrains
    {
    }
}