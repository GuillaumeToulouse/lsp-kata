using System.Collections.Generic;
using NUnit.Framework;
using src;

namespace Tests
{
    public class CamerasTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
           
           var cameras = new List<Camera>();
           var cameraNC1 = new CameraNC1("Front camera");
           cameras.Add(cameraNC1);
           var cameraShower = new CameraShower(cameraNC1);
           Assert.That (cameraShower.Show(), Is.EqualTo("Showing: Front camera"));
 
 
           var cameraNC2 = new CameraNC2("Back camera");
           cameras.Add(cameraNC2);
           cameraShower = new CameraShower(cameraNC2);
           Assert.That (cameraShower.Show(), Is.EqualTo("Showing: Back camera"));
 
           var unsupportedCamera = new UnsupportedCamera(null);
           cameras.Add(unsupportedCamera);
           cameraShower = new CameraShower(unsupportedCamera);
           Assert.That (cameraShower.Show(), Is.EqualTo("Showing: not supported"));      
        }
    }
}